{
    "id": "d0a02998-6b08-4850-89f4-f841c1bc3be0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 90,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbaaf8e6-cd46-4576-9aea-6a80efbd667b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a02998-6b08-4850-89f4-f841c1bc3be0",
            "compositeImage": {
                "id": "381f9c55-c9d9-4def-afb4-ebafd43bf910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbaaf8e6-cd46-4576-9aea-6a80efbd667b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae66408f-6f26-4a9b-8e47-e2d924159bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbaaf8e6-cd46-4576-9aea-6a80efbd667b",
                    "LayerId": "d978e572-ee05-46b1-a811-55b1e8a9ee6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "d978e572-ee05-46b1-a811-55b1e8a9ee6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0a02998-6b08-4850-89f4-f841c1bc3be0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 91,
    "xorig": 45,
    "yorig": 125
}