{
    "id": "8a6ae3b0-89ec-4938-833e-fcc3efb245cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStar_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d873e502-3ef5-4b35-b3cc-01375692ab20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a6ae3b0-89ec-4938-833e-fcc3efb245cc",
            "compositeImage": {
                "id": "154dc34c-28f6-4f2d-aac6-0c861e445cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d873e502-3ef5-4b35-b3cc-01375692ab20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43c2af9e-2172-47b4-a15c-6875404dd7e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d873e502-3ef5-4b35-b3cc-01375692ab20",
                    "LayerId": "44f6b274-71c4-4c74-ab06-400f1ce31edb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44f6b274-71c4-4c74-ab06-400f1ce31edb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a6ae3b0-89ec-4938-833e-fcc3efb245cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}