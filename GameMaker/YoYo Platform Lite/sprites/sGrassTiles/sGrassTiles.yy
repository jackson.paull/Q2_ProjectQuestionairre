{
    "id": "cd8f3d50-abce-469c-90e4-353b67ba21d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrassTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 895,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "886ceb08-67f1-48b2-b96d-48b5caecd085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd8f3d50-abce-469c-90e4-353b67ba21d7",
            "compositeImage": {
                "id": "604777f9-e4c7-4ecc-baac-0bf9d060077a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886ceb08-67f1-48b2-b96d-48b5caecd085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55867825-7114-47a9-9a71-3d73ab9c74e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886ceb08-67f1-48b2-b96d-48b5caecd085",
                    "LayerId": "35f86977-2555-49cc-9b62-fd25afd26fe3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "35f86977-2555-49cc-9b62-fd25afd26fe3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd8f3d50-abce-469c-90e4-353b67ba21d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 896,
    "xorig": 0,
    "yorig": 0
}