{
    "id": "ffb8526c-b813-4b55-921f-a52313148b9d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "6fdaf357-9566-4bd9-adff-b33c89f042e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        },
        {
            "id": "a009f4d4-2d4f-4ed4-a1a4-2f074503401a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        },
        {
            "id": "235e68bc-8732-420d-aae2-bda024f3d366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b4938cbf-d60f-4c97-8bfe-b7b1c9c08b0f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        },
        {
            "id": "d7600900-0a99-40e6-9c75-23083a9f3e94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "40f14080-524e-416a-8d6c-e806c8d7cbf0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        },
        {
            "id": "5afe94e4-db45-4fc0-a361-7e85bd23361b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "605ecff9-e5ac-44b9-b9c8-18b0c4bdb826",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        },
        {
            "id": "7e4cf6e4-d340-490c-93b9-d94d2ebc4e42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        },
        {
            "id": "7abf1476-0e5b-4fc0-8bd8-5f7ea06812c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ffb8526c-b813-4b55-921f-a52313148b9d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
    "visible": true
}