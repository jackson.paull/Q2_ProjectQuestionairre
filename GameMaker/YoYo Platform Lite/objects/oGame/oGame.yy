{
    "id": "72c39378-8268-4c2c-9e1f-c03d3ac8154c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGame",
    "eventList": [
        {
            "id": "6faec06f-83dd-4bad-8ac0-93d609439c40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72c39378-8268-4c2c-9e1f-c03d3ac8154c"
        },
        {
            "id": "67baed5c-3f9d-41cb-b37c-8f729ff0ae44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 10,
            "m_owner": "72c39378-8268-4c2c-9e1f-c03d3ac8154c"
        },
        {
            "id": "d3603a7e-824c-4765-8084-516c32e3ac79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "72c39378-8268-4c2c-9e1f-c03d3ac8154c"
        },
        {
            "id": "103097ca-bc28-4573-bb12-80d6044d3d5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "72c39378-8268-4c2c-9e1f-c03d3ac8154c"
        },
        {
            "id": "52e42677-f053-4a9a-81c4-09ef2708e2fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 89,
            "eventtype": 10,
            "m_owner": "72c39378-8268-4c2c-9e1f-c03d3ac8154c"
        },
        {
            "id": "4d3d3c26-0911-4494-b4b0-1f20733b5c29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "72c39378-8268-4c2c-9e1f-c03d3ac8154c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}