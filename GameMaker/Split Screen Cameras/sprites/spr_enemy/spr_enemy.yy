{
    "id": "4931a9d0-1049-4819-9b1d-85d88e4064de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 32,
    "bbox_right": 80,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 164,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71f49ce5-fd0b-4439-ab56-93ed413e0f1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4931a9d0-1049-4819-9b1d-85d88e4064de",
            "compositeImage": {
                "id": "692df147-ddea-462f-be7d-718dc815dae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71f49ce5-fd0b-4439-ab56-93ed413e0f1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "067d1c00-27c4-46fe-a013-5b1e4ec41cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f49ce5-fd0b-4439-ab56-93ed413e0f1e",
                    "LayerId": "f0ff99d3-ba07-4c03-b3e7-510ec584d7ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "f0ff99d3-ba07-4c03-b3e7-510ec584d7ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4931a9d0-1049-4819-9b1d-85d88e4064de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}