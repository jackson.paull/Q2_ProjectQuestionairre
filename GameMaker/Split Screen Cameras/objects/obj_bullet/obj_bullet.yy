{
    "id": "e8362093-fb65-47c0-84c9-daf06eeeb7b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "89b33106-db92-4f52-9880-309d690689a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "628fe2f4-e632-45a5-ae77-8d241feb11c1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e8362093-fb65-47c0-84c9-daf06eeeb7b0"
        },
        {
            "id": "4de3de47-e781-4779-a24f-784f083a706f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8362093-fb65-47c0-84c9-daf06eeeb7b0"
        },
        {
            "id": "64e5b413-2075-41d0-90df-c742a6f49565",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e8362093-fb65-47c0-84c9-daf06eeeb7b0"
        },
        {
            "id": "3c7a7f0f-e75b-446f-ad87-d08400a0c7e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e8362093-fb65-47c0-84c9-daf06eeeb7b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8b8741a-0b4f-449c-81fd-a3052997d43e",
    "visible": true
}